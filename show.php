<!DOCTYPE html>
<html>
<head>
    <title></title>
<style type="text/css">
        .outerdiv {
            position: absolute;
            top: 0;
            left: 0;
            right: 5em;
        }
        .innerdiv {
            width: 100%;
            overflow-x:scroll;
            margin-left: 5em;
            overflow-y:visible;
            padding-bottom:1px;
        }
        .headcol {
            position:absolute;
            width:5em;
            left:0;
            top:auto;
            border-right: 0px none black;
            border-top-width:3px;
            /*only relevant for first row*/
            margin-top:-3px;
            /*compensate for top border*/
        }
        
</style>
</head>

<body>


<?php
session_start();
ini_set('max_execution_time', 300);
if(!(isset($_SESSION['login'])) && !($_SESSION['login'])){
    header('Location: index.php');
}

$json = file_get_contents('http://api.audiatur.de/webservice/dbjacob/?source=clients');

$obj  = json_decode($json);
$obj  = (array)$obj;
$temp_array = $obj['objects'];
//echo "<pre>";print_r($_SESSION['username']);die('po');

for($i=0; $i<sizeof($temp_array); $i++){

    if(strpos($obj['objects'][$i]->Username, ';')){
        $array   = explode($obj['objects'][$i]->Username, ';');
    }else{
        $array[] = $obj['objects'][$i]->Username;
    }

    if(!in_array($_SESSION['username'], $array)){
        unset($obj['objects'][$i]);
    }
}

$obj['objects'] = array_values($obj['objects']);

?>
<div class="outerdiv">
    <div class="innerdiv">
<table id="" class="" cellspacing="0" width="100%">
    <thead>
        <?php
            if($_SESSION['lang'] == 'eng'){
        ?>  
            <tr>
                <td class="headcol"></td>
                <th>Custnr</th>
                <th>ClientSince</th>
                <th>Leadsource</th>
                <th>Gross</th>
                <th>Vacantpos</th>
                
                <th>Statusen</th>
                <th>Posen</th>
                <th>Titleen</th>
                <th>Todoen</th>
                <th>Historyen</th>
                <th>Notesen</th>
                
                <th>Marketgroup</th>
                <th>Devicenr</th>
                <th>Servicenr</th>
                <th>Company</th>
                <th>Title</th>
                <th>Firstname</th>
                <th>Name</th>
                <th>Pos</th>
                <th>Street</th>
                <th>Zip</th>
                <th>Town</th>
                <th>Country</th>
                <th>Companytel</th>

            </tr>

            <?php
                foreach($obj['objects'] as $array){
            ?>
                <form return="false">
                <tr>

                    <td class="headcol"><input type="submit" value="Update" /></td>
                    <td><input type="text" name="Custnr" value="<?php echo $array->Custnr;?>" /></td>
                    <td><input type="text" name="ClientSince" value="<?php echo $array->ClientSince;?>" /></td>
                    <td><input type="text" name="Leadsource"    value="<?php echo $array->Leadsource;?>" /></td>
                    <td><input type="text" name="Gross"  value="<?php echo $array->Gross;?>" /></td>
                    <td><input type="text" name="Vacantpos"   value="<?php echo $array->Vacantpos;?>" /></td>

                    <td><input type="text" name="Statusen" value="<?php echo $array->Statusen;?>" /></td>
                    <td><input type="text" name="Posen"   value="<?php echo $array->Posen;?>" /></td>
                    <td><input type="text" name="Titleen"  value="<?php echo $array->Titleen;?>" /></td>
                    <td><input type="text" name="Todoen"   value="<?php echo $array->Todoen;?>" /></td>
                    <td><input type="text" name="Historyen" value="<?php echo $array->Historyen;?>" /></td>
                    <td><input type="text" name="Notesen"   value="<?php echo $array->Notesen;?>" /></td>
                    <td><input type="text" name="Marketgroup"  value="<?php echo $array->Marketgroup;?>" /></td>
                    <td><input type="text" name="Devicenr"   value="<?php echo $array->Devicenr;?>" /></td>
                    <td><input type="text" name="Servicenr" value="<?php echo $array->Servicenr;?>" /></td>
                    <td><input type="text" name="Company"   value="<?php echo $array->Company;?>" /></td>
                    <td><input type="text" name="Title"   value="<?php echo $array->Title;?>" /></td>
                    <td><input type="text" name="Firstname"   value="<?php echo $array->Firstname;?>" /></td>
                    <td><input type="text" name="Name" value="<?php echo $array->Name;?>" /></td>
                    <td><input type="text" name="Pos"   value="<?php echo $array->Pos;?>" /></td>
                    <td><input type="text" name="Street"  value="<?php echo $array->Street;?>" /></td>
                    <td><input type="text" name="Zip"   value="<?php echo $array->Zip;?>" /></td>
                    <td><input type="text" name="Town" value="<?php echo $array->Town;?>" /></td>
                    <td><input type="text" name="Country"   value="<?php echo $array->Country;?>" /></td>
                    <td><input type="text" name="Companytel"   value="<?php echo $array->Companytel;?>" /></td>


                </tr> 
                </form>
            <?php }?>
        <?php 
            }elseif($_SESSION['lang'] == 'ger'){
        ?>
            <tr>

                <td class="headcol"></td>
                <th> KD-Nr.</th>
                <th>Kd. seit</th>
                <th>Leadsource</th>
                <th>Umsatz</th>
                <th>OP</th>



                <th>statusde</th>
                <th>pos</th>
                <th>title</th>
                <th>todode</th>
                <th>historyde</th>
                <th>notesde</th>


                <th>Marktgruppe</th>
                <th>Gerätenr.</th>
                <th>Händlernummer</th>
                <th>Company</th>
                <th>Anrede</th>
                <th>Vorname</th>
                <th>Name</th>
                <th>Position</th>
                <th>Adresse</th>
                <th>plz</th>
                <th>Stadt</th>
                <th>Land</th>
                <th>Zentrale</th>

                <th></th>
            </tr>

            <?php
                foreach($obj['objects'] as $array){
            ?>
                <form return="false">
                <tr>
                    <td class="headcol"><input type="submit" value="Update" /></td>
                    <td><input type="text" name="Custnr" value="<?php echo $array->Custnr;?>" /></td>
                    <td><input type="text" name="ClientSince" value="<?php echo $array->ClientSince;?>" /></td>
                    <td><input type="text" name="Leadsource"    value="<?php echo $array->Leadsource;?>" /></td>
                    <td><input type="text" name="Gross"  value="<?php echo $array->Gross;?>" /></td>
                    <td><input type="text" name="Vacantpos"   value="<?php echo $array->Vacantpos;?>" /></td>


                    <td><input type="text" name="Statusde" value="<?php echo $array->Statusde;?>" /></td>
                    <td><input type="text" name="Pos"    value="<?php echo $array->Pos;?>" /></td>
                    <td><input type="text" name="Title"  value="<?php echo $array->Title;?>" /></td>
                    <td><input type="text" name="Todode"   value="<?php echo $array->Todode;?>" /></td>
                    <td><input type="text" name="Historyde" value="<?php echo $array->Historyde;?>" /></td>
                    <td><input type="text" name="Notesde"   value="<?php echo $array->Notesde;?>" /></td>


                    <td><input type="text" name="Marketgroup"  value="<?php echo $array->Marketgroup;?>" /></td>
                    <td><input type="text" name="Devicenr"   value="<?php echo $array->Devicenr;?>" /></td>
                    <td><input type="text" name="Servicenr" value="<?php echo $array->Servicenr;?>" /></td>
                    <td><input type="text" name="Company"   value="<?php echo $array->Company;?>" /></td>
                    <td><input type="text" name="Title"   value="<?php echo $array->Title;?>" /></td>
                    <td><input type="text" name="Firstname"   value="<?php echo $array->Firstname;?>" /></td>
                    <td><input type="text" name="Name" value="<?php echo $array->Name;?>" /></td>
                    <td><input type="text" name="Pos"   value="<?php echo $array->Pos;?>" /></td>
                    <td><input type="text" name="Street"  value="<?php echo $array->Street;?>" /></td>
                    <td><input type="text" name="Zip"   value="<?php echo $array->Zip;?>" /></td>
                    <td><input type="text" name="Town" value="<?php echo $array->Town;?>" /></td>
                    <td><input type="text" name="Country"   value="<?php echo $array->Country;?>" /></td>
                    <td><input type="text" name="Companytel"   value="<?php echo $array->Companytel;?>" /></td>

                </tr> 
                </form>
            <?php }?>
        <?php 
            }
        ?>
        <tr>
            
        </tr>
    </thead>
</table>
<?php
/*foreach($obj['objects'] as $array){

}*/
//echo "<pre>";print_r($obj);die;
?>
</div></div>

</body>
</html>
