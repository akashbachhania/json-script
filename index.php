<?php
if(isset($_REQUEST['submit'])){

    $err = '';
    $json = file_get_contents('http://www.audiatur.de/media/controlPDF_L1ASumT.json');
    
    $obj = json_decode($json);
    $obj = (array)$obj;

    $web_access = (array)$obj['web-access'];
    $key = array_search($_REQUEST['username'], $web_access);

    if($key){
        preg_match_all('!\d+!', $key, $matches);

        if($web_access['pw'.$matches[0][0]] == $_REQUEST['password']){
            session_start();
            $_SESSION['login']      =   true;
            $_SESSION['username']   =   $_REQUEST['username'];
            $_SESSION['lang']       =   $_REQUEST['lang'];
            header('Location: show.php');
        }
        else{
            $err = "Username or Password does not match";
        }
        //$matches[0][0]    
    }else{
        $err = "Username or Password does not match";
    }
}
//echo "<pre>";print_r($obj['web-access']);
?>
<!DOCTYPE html>
<html>
  <head>

    <meta charset="UTF-8">
    <title>Auditor Login Form</title>
    <link rel="stylesheet" href="css/reset.css">
    <link rel='stylesheet prefetch' href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900|RobotoDraft:400,100,300,500,700,900'>
    <link rel='stylesheet prefetch' href='http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css'>
    <link rel="stylesheet" href="css/style.css">

  </head>

<body>
<!-- Mixins-->
<!-- Pen Title-->
<div class="pen-title">
  <h1>Auditor Login Form</h1>
</div>

<form method="post" action="">
<div class="container">
  <div class="card"></div>
  <div class="card">
    <select style="float:right" name="lang">
        <option value="ger">German</option>
        <option value="eng">English</option>
    </select>
    <h1 class="error"><?php echo isset($err) ? $err : ''; ?> </h1>
    <h1 class="title">Login</h1>
      <div class="input-container">
        <input type="password" name="username">
        <label for="Username">Username</label>
        <div class="bar"></div>
      </div>
      <div class="input-container">
        <input type="password" name="password">
        <label for="Password">Password</label>
        <div class="bar"></div>
      </div>
      <div class="button-container">
        <button name="submit"><span>Go</span></button>
      </div>
      <div class="footer"><a href="#">Forgot your password?</a></div>
  </div>
  </form>
  <!-- <div class="card alt">
    <div class="toggle"></div>
    <h1 class="title">Register
      <div class="close"></div>
    </h1>
    <form>
      <div class="input-container">
        <input type="text" id="Username" required="required"/>
        <label for="Username">Username</label>
        <div class="bar"></div>
      </div>
      <div class="input-container">
        <input type="password" id="Password" required="required"/>
        <label for="Password">Password</label>
        <div class="bar"></div>
      </div>
      <div class="input-container">
        <input type="password" id="Repeat Password" required="required"/>
        <label for="Repeat Password">Repeat Password</label>
        <div class="bar"></div>
      </div>
      <div class="button-container">
        <button><span>Next</span></button>
      </div>
    </form>
  </div> -->
</div>

<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script src="js/index.js"></script>

</body>
</html>
